var tabView = document.querySelector(".tab-view");
var tabs = tabView.querySelectorAll(".tab");


for(var i = 0; i < tabs.length; i++){
    var tab = tabs[i];

    tab.addEventListener("click", function(){
        var activeTab = tabView.querySelector(".tab[active]");
        var activeTabPanel = document.querySelector(".tab-panel[id=" + activeTab.getAttribute("for") + "]");
        activeTab.removeAttribute("active");
        activeTabPanel.removeAttribute("show");

        this.setAttribute("active","");
        var panel = document.querySelector(".tab-panel[id=" + this.getAttribute("for") + "]");
        panel.setAttribute("show","");
    });
}

chrome.browserAction.setBadgeBackgroundColor({color: [0,160,0,255]});

var timetable = [];
var classes = [];
var tabela;
var req = new XMLHttpRequest();
req.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
        var list = req.responseText.match(/<ul>(.*?)<\/ul>/gs)[0];
        var match;
        var regex = new RegExp(/<li><a href.*?o(.*?)\.html".*?>(.*?)<\/a><\/li>/gs);
        while(match = regex.exec(list)){
            klasa.innerHTML += "<option value='" + match[1] + "'>" + match[2] + "</option>";
            classes.push([match[1],match[2]]);
        }

        
        chrome.storage.sync.get('klasa', function (result) {
            if(Object.keys(result).length !== 0){
                klasa.value = result["klasa"];
                chrome.browserAction.setBadgeText({text: classes[result["klasa"]-1][1]});
                Load();
            } else {
                klasa.value = 1;
                Load();
            }
        });

    }
};
req.open("GET", "http://www.zseil.edu.pl/rnowa/html/lista.html", true);
req.send(null);

klasa.addEventListener("change",function(){
    chrome.storage.sync.set({ klasa: klasa.value });
    chrome.browserAction.setBadgeText({text: classes[klasa.value-1][1]});
    Load();
});

function Load(){
    var req2 = new XMLHttpRequest();
    req2.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var data = req2.responseText;
            var table = data.match(/(<table border="1" cellspacing="0" cellpadding="4" class="tabela">.*?<\/table>)/gs);
            var timeRegex = new RegExp(/<td class="nr">([0-9]+)<\/td>.*?<td class="g">\s?([0-9]+):([0-9]+)-\s?([0-9]+):([0-9]+)<\/td>/gs);
            var match;

            while(match = timeRegex.exec(table)){
                timetable.push({nr: match[1],sh: match[2],sm: match[3],st: match[2]*3600+match[3]*60,eh: match[4],em: match[5],et: match[4]*3600+match[5]*60});
            }       

            plan.innerHTML = table;
            tabela = plan.getElementsByClassName("tabela")[0];

            UpdateRemainingTime();
        }
    };

    req2.open("GET", "http://www.zseil.edu.pl/rnowa/html/plany/o" + klasa.options[klasa.selectedIndex].value + ".html", true);
    req2.send(null);
}

setInterval(UpdateRemainingTime,1000);

function UpdateRemainingTime() {
    var date = new Date();
    var weekday = date.getDay();
    var time = date.getHours() * 3600 + date.getMinutes() * 60 + date.getSeconds();
    var endTime = 0;
    var remainingTime = 0;
    var lessonNum = 0;

    for(var i = 0;i < timetable.length;i++){
        if(timetable[i].et > time){
            lessonNum = timetable[i].nr;
            endTime = timetable[i].et;
            break;
        }
    }

    if(lessonNum == 0 || remainingTime - 45*60 < 0) {
        czas.innerHTML = "Poza lekcjami";
    } else {
        remainingTime = endTime - time;

        if(remainingTime > 45*60){
            remainingTime = remainingTime - 45*60;
            czas.innerHTML = "Do końca przerwy pozostało " + TimeFormat(remainingTime,"<b>{1}</b><unit>min</unit> <b>{2}</b><unit>s</unit>");
        } else {
            czas.innerHTML = "Do końca lekcji pozostało " + TimeFormat(remainingTime,"<b>{1}</b><unit>min</unit> <b>{2}</b><unit>s</unit>");
            if(remainingTime < 2){
                tabela.getElementsByTagName("tr")[lessonNum].getElementsByTagName("td")[weekday+1].style.background = "none";
            } else {
                tabela.getElementsByTagName("tr")[lessonNum].getElementsByTagName("td")[weekday+1].style.background = "linear-gradient(to right,#aaf7aa " + (100-(remainingTime/(45*60))*100) + "%,#dfd 0%)";
            }
        }
    }
}

function TimeFormat(time,format){
    var hours = Math.floor(time/3600);
    var minutes = Math.floor(time/60 - hours * 3600);
    var seconds = Math.floor(time - minutes * 60);

    return format.format(hours,minutes,seconds);
}

String.prototype.format = function() {
    a = this;
    for (k in arguments) {
        a = a.replace("{" + k + "}", arguments[k])
    }
    return a
}

var date = new Date();
var dateString = date.getFullYear() + "-" + (date.getMonth()+1) + "-" + date.getDate();
var dayName = getDayName(date.getDay());

var req3 = new XMLHttpRequest();
req3.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        var data = req3.responseText;
        alert(data);
    }
};

req3.open("GET",  "http://zseil.edu.pl/zastepstwa/uploads/" + dateString + "_" + dayName + ".html", true);
req3.send(null);

function getDayName(day){
    switch(day){
        case 1:
            return "Poniedzialek";
            break;
        case 2:
            return "Wtorek";
            break;
        case 3:
            return "Sroda";
            break;
        case 4:
            return "Czwartek";
            break;
        case 5:
            return "Piatek";
            break;
    }
}